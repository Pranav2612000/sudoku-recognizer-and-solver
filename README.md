# Sudoku Recognizer and Solver

Takes a sudoku image as input and displays the solved sudoku. Uses Tkinter for graphical interface

# Technologies Used:
<ul>
<li>Python Programming Language
<li>Tkinter for Graphical Interface
<li>OpenCV for image preprocessing and image manipulation
<li>tesseract(Pytesseract) for digit recognition
</ul>


# Requirements:
<ul>
<li>OpenCV for Python
<li>Tkinter Python
<li>pytesseract
<li>numpy
<li>Pillow

# To Do

Tesseract 4 does not support whitelist feature, so it is difficult to limit tesseract to recognize digits, so I have written a function which converts letters to digits(e.g. 'B' to 8, 'g' to 9 etc). This can be removed when whitelist feature is available.

Use a Machine Learning method for digit recognition in which the dataset is updated and the algorithm improved every time a new image is checked.